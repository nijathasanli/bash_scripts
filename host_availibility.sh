#!/bin/bash
for i in {10..20};
do 
    if  ping -c 3 -n -W 2  192.168.92.$i >/dev/null;then
        echo "192.168.92.$i is UP!"
    else
        echo "192.168.92.$i is DOWN!"
    fi
done
exit

# Create Telegram bot
# curl "https://api.telegram.org/bot<your access_token>/getUpdates"
# Get username id: JSON dump bot
#/usr/local/bin/
#!/bin/bash
 ACCESS_TOKEN="<access_token>"
 URL="https://api.telegram.org/bot$ACCESS_TOKEN/sendMessage"
 CHAT_ID="<telegram_chat_id>" # Telegram id from /getUpdates
 MESSAGE="*Login Alert*: $(date '+%Y-%m-%d %H:%M:%S %Z')
 username: $PAM_USER
 hostname: $HOSTNAME
 remote host: $PAM_RHOST
 remote user: $PAM_RUSER
 service: $PAM_SERVICE
 tty: $PAM_TTY"
 PAYLOAD="chat_id=$CHAT_ID&text=$MESSAGE&disable_web_page_preview=true&parse_mode=Markdown"
 curl -s --max-time 13 --retry 3 --retry-delay 3 --retry-max-time 13 -d "$PAYLOAD" $URL > /dev/null 2>&1 &

#/etc/pam.d/sshd
#session required pam_exec.so type=open_session seteuid /usr/local/bin/telegramAuth-bot.sh
#systemctl restart sshd